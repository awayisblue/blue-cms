const error = require('../utils/error-store')
const config = require('config')
const jwt = require('jsonwebtoken')
module.exports = function () {
  return async (ctx, next) => {
    let sid = ctx.get('Authorization')
    if (!sid) throw error.UNAUTHORIZED
    let token = sid.split(/\s+/).pop()
    let session
    try {
      session = jwt.verify(token, config.jwtSecret)
    } catch (err) {
      throw error.UNAUTHORIZED
    }
    ctx.state.session = session
    return next()
  }
}
