const config = require('config')
const http = require('http')
const HttpProxy = require('http-proxy')
const proxy = new HttpProxy()
const koaApp = require('./koa')
const koaHandler = koaApp.callback()
const redisDB = require('./clients/redis')
const redisKeyStore = require('./utils/redis-key-store')
const logger = require('./utils/logger')
const error = require('./utils/error-store')
const preservedPath = ['_mount-point']
const path = require('path')
const server = http.createServer(function (req, res) {
  (async function run () {
    let mountPoint = req.headers['x-mount-point']
    let pathWithoutPrefix = req.url.split(config.server.prefix)[1] // /path-without-prefix/next/
    let toKoa = true
    if (mountPoint && pathWithoutPrefix && preservedPath.indexOf(pathWithoutPrefix.split('/')[1]) > -1) {
      // 目前使用了 host, allowCMS, mountPointToken 3个字段
      let mountPointConfig = await redisDB.hgetall(redisKeyStore.gatewayMountPoint(mountPoint))
      // 需要开启cms配置且配置了token
      if (mountPointConfig.allowCMS === 'Y' && mountPointConfig.mountPointToken) {
        toKoa = false
        // 改变mountPoint再转发请求
        req.url = path.join('/', mountPoint, pathWithoutPrefix)
        proxy.web(req, res, {
          target: config.gatewayHost,
          headers: {
            'x-mount-point-token': mountPointConfig.mountPointToken // 每个mountPoint如果要使用
          }
        })
      }
    }
    if (toKoa) {
      koaHandler(req, res)
    }
  })()
}).listen(config.server.port || 3000)
server.on('error', (e) => {
  logger.error(e)
})
proxy.on('error', (e, req, res) => {
  logger.error(e)
  let errorRes = error.ACCESS_MOUNT_POINT_ERROR
  res.writeHead(errorRes.statusCode, {
    'Content-Type': 'application/json'
  })
  res.end(JSON.stringify(errorRes.data))
})
koaApp.on('error', function (err) {
  logger.error(err)
})
module.exports = server
