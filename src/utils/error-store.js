let errCode = 1000
module.exports = {
  commonStatus: {
    '200': {
      statusCode: 200,
      data: {
        code: 0,
        msg: 'OK'
      }
    },
    '301': {
      statusCode: 301,
      data: {
        code: 301,
        msg: 'Moved Permanently'
      }
    },
    '302': {
      statusCode: 302,
      data: {
        code: 302,
        msg: 'Redirecting'
      }
    },
    '404': {
      statusCode: 404,
      data: {
        code: 404,
        msg: 'Not Found'
      }
    },
    '403': {
      statusCode: 403,
      data: {
        code: 403,
        msg: 'Forbidden'
      }
    }
  },
  OK: {
    statusCode: 200,
    data: {
      code: 0,
      msg: 'OK'
    }
  },
  PARAM_ERROR: {
    statusCode: 400,
    data: {
      code: errCode++,
      msg: '参数错误'
    }
  },
  SERVER_ERROR: {
    statusCode: 500,
    data: {
      code: errCode++,
      msg: '服务器错误'
    }
  },
  UNAUTHORIZED: {
    statusCode: 403,
    data: {
      code: errCode++,
      msg: '未登录'
    }
  },
  ACCESS_MOUNT_POINT_ERROR: {
    statusCode: 500,
    data: {
      code: errCode++,
      msg: '访问挂载点出错'
    }
  },
  MOUNT_POINT_EXISTS: {
    statusCode: 400,
    data: {
      code: errCode++,
      msg: '挂载点被占用'
    }
  },
  MOUNT_POINT_NOT_EXISTS: {
    statusCode: 400,
    data: {
      code: errCode++,
      msg: '访问挂载点出错'
    }
  },
  ACCOUNT_NOT_EXISTS: {
    statusCode: 400,
    data: {
      code: errCode++,
      msg: '账号不存在，用户名或密码错误'
    }
  }
}
