const config = require('config')
function compose (...rest) {
  return rest.join(':')
}
// 综合管理redis的key
// let appName = config.appName || '__cms'
let getewayAppName = config.gatewayAppName || '__default'
module.exports.gatewayMountPoint = function (mountPoint) {
  return compose(getewayAppName, '__mountPoint', mountPoint)
}
