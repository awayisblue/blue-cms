const Router = require('koa-router')
const mountPoint = require('./_mount-point/route')
const account = require('./account/route')
const session = require('../middlewares/session')
const config = require('config')
const router = new Router({prefix: config.server.prefix || '/'})
router.use('/account', account.routes())
router.use('/_mount-point', session(), mountPoint.routes())
module.exports = router
