const Router = require('koa-router')
const router = new Router()
const validate = require('validate.js')
const error = require('../../utils/error-store')
const tool = require('../../utils/tool')
const redisDB = require('../../clients/redis')
const redisKeyStore = require('../../utils/redis-key-store')
// 提供配置相关的api，供前端请求配置
router.get('/menus', async function (ctx, next) {
  ctx.body = {
    list: [
      {
        name: '挂载点列表',
        desc: '',
        path: '/mount-points', // 相对于 _mount-point的path
        methods: ['get', 'post'], // 支持get, post ,put, delete
        pagination: false, // 是否支持分页
        pageKey: 'page', // 页码
        amountKey: 'amount', // 单页数据量
        fields: {
          mountPoint: {
            presence: {allowEmpty: false},
            format: {
              pattern: '^\\w+$'
            }, // 只支持英文字符
            fieldInfo: {
              name: '挂载点',
              desc: '',
              noEdit: true // 不支持修改
            }
          },
          host: {
            presence: {allowEmpty: false},
            fieldInfo: {
              name: '请求域名',
              desc: '如：http://localhost'
            }
          },
          desc: {
            presence: false,
            fieldInfo: {
              name: '描述',
              desc: ''
            }
          },
          allowCMS: {
            presence: false,
            inclusion: ['Y', 'N'],
            fieldInfo: {
              name: '支持配置(Y/N)',
              desc: '配置是否支持在cms里进行自定义配置'
            }
          },
          mountPointToken: {
            presence: false,
            fieldInfo: {
              name: '挂载点token',
              desc: 'cms进行资源管理时，请求的token'
            }
          }
        }
      }
    ]
  }
})
// 查所有mount-points
router.get('/mount-points', async function (ctx, next) {
  let pattern = redisKeyStore.gatewayMountPoint('*')
  let keys = await redisDB.keys(pattern)
  let result = keys.map((key) => {
    let mountPoint = key.split(':').pop()
    return {
      name: mountPoint,
      methods: ['get', 'put', 'delete'],
      path: `/mount-points/${mountPoint}`
    }
  })
  ctx.body = {
    list: result
  }
})
// 新增mount-point
router.post('/mount-points', async function (ctx, next) {
  // host, allowCMS, mountPointToken
  const constrains = {
    mountPoint: { // 挂载点
      presence: {allowEmpty: false},
      format: /^\w+$/ // 只支持英文字符
    },
    host: {
      presence: {allowEmpty: false}
    },
    desc: { // 描述
      presence: false
    },
    allowCMS: { // 是否支持cms配置
      presence: false,
      inclusion: ['Y', 'N']
    },
    mountPointToken: { // cms进行资源管理时，请求的token
      presence: false
    }
  }
  let postBody = ctx.request.body
  if (validate(postBody, constrains)) {
    throw error.PARAM_ERROR
  }
  let data = tool.filterPropertys(postBody, Object.getOwnPropertyNames(constrains))
  data['create_at'] = (new Date()).getTime()
  let redisKey = redisKeyStore.gatewayMountPoint(data.mountPoint)
  let exists = await redisDB.exists(redisKey)
  if (exists) throw error.MOUNT_POINT_EXISTS
  await redisDB.hmset(redisKey, data)
  ctx.body = {}
})
// 查某个mount-point
router.get('/mount-points/:name', async function (ctx, next) {
  let mountPoint = ctx.params.name
  let mountPointConfig = await redisDB.hgetall(redisKeyStore.gatewayMountPoint(mountPoint))
  ctx.body = {
    data: mountPointConfig
  }
})
// 修改某个mount-point
router.put('/mount-points/:name', async function (ctx, next) {
  const constrains = {
    host: {
      presence: {allowEmpty: false}
    },
    desc: { // 描述
      presence: false
    },
    allowCMS: { // 是否支持cms配置
      presence: false,
      inclusion: ['Y', 'N']
    },
    mountPointToken: { // cms进行资源管理时，请求的token
      presence: false
    }
  }
  let postBody = ctx.request.body
  if (validate(postBody, constrains)) {
    throw error.PARAM_ERROR
  }
  let data = tool.filterPropertys(postBody, Object.getOwnPropertyNames(constrains))
  let mountPoint = ctx.params.name
  let redisKey = redisKeyStore.gatewayMountPoint(mountPoint)
  let exists = await redisDB.exists(redisKey)
  if (!exists) throw error.MOUNT_POINT_NOT_EXISTS
  await redisDB.hmset(redisKey, data)
  ctx.body = {}
})
// 删除某个mount-point
router.del('/mount-points/:name', async function (ctx, next) {
  let mountPoint = ctx.params.name
  let redisKey = redisKeyStore.gatewayMountPoint(mountPoint)
  let exists = await redisDB.exists(redisKey)
  if (!exists) throw error.MOUNT_POINT_NOT_EXISTS
  await redisDB.del(redisKey)
  ctx.body = {}
})
module.exports = router
