const Router = require('koa-router')
const router = new Router()
const validate = require('validate.js')
const error = require('../../utils/error-store')
const config = require('config')
const jwt = require('jsonwebtoken')
// 登入
router.post('/login', async function (ctx, next) {
  const constrains = {
    username: {
      presence: {allowEmpty: false}
    },
    password: {
      presence: {allowEmpty: false}
    }
  }
  let postBody = ctx.request.body
  if (validate(postBody, constrains)) {
    throw error.PARAM_ERROR
  }
  // 目前只支持一个账号登录，账号管理功能暂时没有
  if (postBody.username !== config.account.username || postBody.password !== config.account.username) {
    throw error.ACCOUNT_NOT_EXISTS
  }
  let token = jwt.sign({}, config.jwtSecret, { expiresIn: '1d' })
  ctx.body = {
    token: token
  }
})
module.exports = router
